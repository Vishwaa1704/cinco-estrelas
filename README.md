# cinco-estrelas
Cinco Estrelas Mobile Application

# How to build Android Signed APK in MAC
1. cd cinco-estrelas
2. ionic cordova build android --release --prod --aot
3. jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore  SYSTEMPATH/cinco-estrelas/portugal.keystore /SYSTEM/cinco-estrelas/platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk portugal
4. keyphrase - portugal
5. ~/Library/Android/sdk/build-tools/27.0.3/zipalign -v 4 app-release-unsigned.apk Cinco.apk     

[SYSTEMPATH- Depends where is the location of project in your system]
