import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from "ionic-angular";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { ActionSheetController } from "ionic-angular";
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 
@Component({
  selector: "page-updateUser",
  templateUrl: "updateUserByAdmin.html"
})
export class UpdateUserByAdminPage {
  imgsrc = "";
  namereadonly: boolean = true;
  emailreadonly: boolean = true;
  anyChange: boolean = false;
  profile: FormGroup;
  settings: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    public loadctrl: LoadingController,
    private formBuilder: FormBuilder,
    private restProvider: RestProvider,
    private userProvider: UserProvider,
    private alertCtrl: AlertController,
  ) {
    this.profile = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])],
      role: ['']
      });
  }

  ngOnInit() {
    this.settings = {
      id: this.navParams.get('id'),
      name: this.navParams.get('name'),
      role: this.navParams.get('role'),
      email: this.navParams.get('email'),
    };
    this.profile.controls.name.setValue(this.settings.name);
    this.profile.controls.email.setValue(this.settings.email);
  }
  nameEdit() {
    this.anyChange = true;
    this.namereadonly = !this.namereadonly;
  }
  emailEdit() {
    this.anyChange = true;
    this.emailreadonly = !this.emailreadonly;
  }
  imgUpdate() {
    this.anyChange = true;
    let actionSheet = this.actionSheetCtrl.create({
      // title: 'TAKE PHOTO',
      buttons: [
        {
          text: "Apagar Foto",
          cssClass: "EditionIcon",
          handler: () => {
            this.deletePhoto();
          }
        },
        {
          text: "Escolher Foto",
          cssClass: "Escolhercss",
          handler: () => {
            this.takePhoto(0);
          }
        },
        {
          text: "Tirar Selfie",
          cssClass: "Tirarcss",
          handler: () => {
            this.takePhoto(1);
          }
        },
        {
          text: "Cancelar",
          cssClass: "cancelCss",
          handler: () => {
          }
        }
      ]
    });

    actionSheet.present();
  }
  takePhoto(val) {
    const options: CameraOptions = {
      quality: 30,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: val
    };

    this.camera.getPicture(options).then(
      imageData => {
        let base64Image = "data:image/jpeg;base64," + imageData;
        //this.updateProfileImage(base64Image);
      },
      err => {
        // Handle error
      }
    );
  }
  deletePhoto() {}

  showError(code, text) {
    let alert = this.alertCtrl.create({
      title: code && code == 409 ? 'User already exists' : text || 'Some error occurred, please try again',
      buttons: [
        {
          text: 'OK',
          role: 'cancel'
        }
      ],
      cssClass: 'custom-alertBox'
    });
    alert.present();
  }


  updateProfile() {
    let loading = this.loadctrl.create({
      spinner: "bubbles"
    });
    loading.present();
    this.restProvider.updateProfileByAdmin(this.profile.value, this.settings.id).subscribe(
      (result) => {
        loading.dismiss();
        this.userProvider.setSettings(result);
        this.showError(200, 'updated sucussefully');
      },
      (error) => {
        loading.dismiss();
        this.showError(500, null);
      }
    )
  }
}
