import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { UpdateUserByAdminPage } from "./updateUserByAdmin";

@NgModule({
  imports: [IonicPageModule.forChild(UpdateUserByAdminPage)]
})
export class UpdateUserPageModule {}
