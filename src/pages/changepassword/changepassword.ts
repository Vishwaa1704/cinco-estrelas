import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from "ionic-angular";
import { RestProvider } from '../../providers/rest/rest';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the ChangepasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 
@Component({
  selector: "page-changepassword",
  templateUrl: "changepassword.html"
})
export class ChangepasswordPage {
  oldpasswordType: any = "password";
  oldpasswordIcon: any = "eye";
  newpasswordType: any = "password";
  newpasswordIcon: any = "eye";
  confirmpasswordType: any = "password";
  confirmpasswordIcon: any = "eye";
  disabled: boolean = true;
  oldPassword;
  newPassword;
  confirmPassword;
  passwordMismatch = false;
  settings;

  constructor(public navCtrl: NavController, public navParams: NavParams, private restProvider: RestProvider, private userProvider: UserProvider,
    private alertCtrl: AlertController, public loadctrl: LoadingController,) {
    this.settings = this.userProvider.getSettings();
  }

  ionViewDidLoad() {
  }
  hideShowPassword(val) {
    if (val == "old") {
      this.oldpasswordType = this.oldpasswordType === "text" ? "password" : "text";
      this.oldpasswordIcon = this.oldpasswordIcon === "eye-off" ? "eye" : "eye-off";
    } else if (val == "new") {
      this.newpasswordType = this.newpasswordType === "text" ? "password" : "text";
      this.newpasswordIcon = this.newpasswordIcon === "eye-off" ? "eye" : "eye-off";
    } else if (val == "confirm") {
      this.confirmpasswordType = this.confirmpasswordType === "text" ? "password" : "text";
      this.confirmpasswordIcon = this.confirmpasswordIcon === "eye-off" ? "eye" : "eye-off";
    }
  }

  showError(code, text) {
    let alert = this.alertCtrl.create({
      title: code && code == 409 ? 'User already exists' : text || 'Some error occurred, please try again',
      buttons: [
        {
          text: 'OK',
          role: 'cancel'
        }
      ],
      cssClass: 'custom-alertBox'
    });
    alert.present();
  }

  confirmcomplete() {
    if (this.oldPassword != "" && this.newPassword != "" && this.confirmPassword != "") {
      if(this.newPassword != this.confirmPassword) {
        this.passwordMismatch = true;
        this.disabled = true;
      } else {
        this.disabled = false;
        this.passwordMismatch = false;
        let loading = this.loadctrl.create({
          spinner: "bubbles"
        });
        loading.present();
        this.restProvider.updateProfile({ password: this.newPassword }, this.settings.id).subscribe(
          (result) => {
            loading.dismiss();
            this.userProvider.setSettings(result);
            this.showError(200, 'updated sucussefully');
          },
          (error) => {
            loading.dismiss();
            this.showError(500, null);
          }
        )
      }
    } else {
      this.disabled = true;
    }
  }
}
