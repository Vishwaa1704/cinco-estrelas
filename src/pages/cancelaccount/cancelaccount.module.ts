import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { CancelaccountPage } from "./cancelaccount";

@NgModule({
 // declarations: [CancelaccountPage],
  imports: [IonicPageModule.forChild(CancelaccountPage)]
})
export class CancelaccountPageModule {}
