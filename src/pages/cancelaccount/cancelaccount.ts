import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { CommentPage } from "../comment/comment";
import { AlertProvider } from '../../providers/alert/alert';
import { RestProvider } from '../../providers/rest/rest';
import { UserProvider } from '../../providers/user/user';
import { RegistrationPage } from "../registration/registration";
 
@Component({
  selector: "page-cancelaccount",
  templateUrl: "cancelaccount.html"
})
export class CancelaccountPage {
  reason = "";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private user: UserProvider,
    private restProvider: RestProvider,
    private alertCtrl: AlertProvider,
  ) {}

  ionViewDidLoad() {
  }
  changeReason() {
    const id = this.user.getUserId()
    if(id && this.reason && this.reason.length > 0) {
      this.restProvider.cancelAccount({ id: id, reason: this.reason })
      .subscribe(
        (result: any) => {
          this.alertCtrl.showError(200, 'Account deleted');
          this.navCtrl.setRoot(RegistrationPage);
        },
        err => {
          this.alertCtrl.showError(err.status, err.statusText);
        }
      )
    }
    
  }
  goToFeedback() {
    this.navCtrl.push(CommentPage, { reason: this.reason });
  }
}
