import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ItemSliding
} from "ionic-angular";
import { UserProvider } from "../../providers/user/user";
// import { User } from "firebase";
import { RestProvider } from "../../providers/rest/rest";
import { AlertProvider } from "../../providers/alert/alert";
import { LocationDetailsPage } from "../locationDetails/locationDetails";
import * as _ from "lodash";

@Component({
  selector: "page-favourites",
  templateUrl: "favourites.html"
})
export class FavouritesPage {
  segment: any;
  tab1;
  tab2;
  tab3;
  listArray = [];
  todoArray = [];
  promoArray = [];
  alimentacoArray = [];
  apoioArray = [];
  allEmpty: boolean = false;
  groupBy = (items, key) =>
    items.reduce(
      (result, item) => ({
        ...result,
        [item[key]]: [...(result[item[key]] || []), item]
      }),
      {}
    );
  favs = {};
  keys = [];
  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private user: UserProvider,
    private restProvider: RestProvider,
    private alertCtrl: AlertProvider,
    public loadctrl: LoadingController
  ) {
    this.listArray = ["1", "2", "3", "4"];
    this.segment = ""; /*
  	this.todoArray=['1','2','3','4'];*/
  }

  ionViewDidEnter() {
    this.loading = this.loadctrl.create({
      spinner: "bubbles"
    });
    this.loading.present();
    this.restProvider
      .getFavs({ ids: this.user.getSettings().favourites })
      .subscribe(
        result => {
          this.favs = this.groupBy(result, "locationType");
          this.keys = Object.keys(this.favs);
          // this.segment = this.keys[0];

          this.loading.dismiss();
        },
        err => {
          this.alertCtrl.showError(err.status, err.statusText);
          this.loading.dismiss();
        }
      );
  }

  ionViewDidLoad() {
    if (
      this.todoArray.length == 0 &&
      this.promoArray.length == 0 &&
      this.alimentacoArray.length == 0 &&
      this.apoioArray.length == 0
    ) {
      this.allEmpty = false;
      this.segment = "";
      this.keys = [];
    }
  }
  deleteItem(item: ItemSliding, locationId, key) {
    this.restProvider
      .removeFromFavs({
        userId: this.user.getUserId(),
        mapId: locationId
      })
      .subscribe(
        result => {
          this.user.removeFromFavs(locationId);
          let val = _.find(this.favs[key], { _id: locationId });
          this.favs[key].splice(val, 1);
        },
        error => {
          this.alertCtrl.showError(error.code, error.message);
        }
      );

  }

  goToLocationDetailPage(location) {
    this.navCtrl.push(LocationDetailsPage, {
      location
    });
  }
}
