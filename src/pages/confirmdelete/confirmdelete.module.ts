import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ConfirmdeletePage } from "./confirmdelete";

@NgModule({
 // declarations: [ConfirmdeletePage],
  imports: [IonicPageModule.forChild(ConfirmdeletePage)]
})
export class ConfirmdeletePageModule {}
