import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, Platform } from "ionic-angular";
import { RestProvider } from '../../providers/rest/rest';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
import { CompanyCommentPage } from '../companyComment/companyComment';
import { MapProvider } from "../../providers/map/map";
import { Geolocation } from "@ionic-native/geolocation";
import { MarkerProvider } from "../../providers/marker/marker";
import { ActivePagesProvider } from '../../providers/activePages/activePages';

@Component({
  selector: "page-locationDetails",
  templateUrl: "locationDetails.html"
})
export class LocationDetailsPage {
  location: any;
  dir: string = "ltr";
  favs = false;
  lat;
  lng;
  sendData: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private restProvider: RestProvider,
    public platform: Platform,
    private user: UserProvider,
    private alertCtrl: AlertProvider,
    public mapProvider: MapProvider,
    private geolocation: Geolocation,
    private markers: MarkerProvider,
    public activePagesProvider: ActivePagesProvider,
  ) {
    this.location = navParams.get("location");
    this.activePagesProvider.setCurrentPage('detailsPage');
    this.dir = platform.dir();
    this.favs = this.user.getSettings().favourites.length > 0 && this.user.getSettings().favourites.indexOf(this.location._id) > -1 || false;
  }

  ionViewDidLoad() {
    this.markers.setShowFilters(false);
    this.initializeMap();
  }

  initializeMap() {
    this.geolocation
      .getCurrentPosition()
      .then(response => {
        this.lat = response.coords.latitude;
        this.lng = response.coords.longitude;
      })
      .catch(error => this.alertCtrl.showError(500, 'Please enable geolocation'));
  }

  addToFav() {
    if (this.favs) {
      this.restProvider
        .removeFromFavs({
          userId: this.user.getUserId(),
          mapId: this.location._id
        })
        .subscribe(
          result => {
            this.favs = false;
            this.user.removeFromFavs(this.location._id);
          },
          error => {
            this.alertCtrl.showError(error.code, error.message);
          }
        );
    } else {
      this.restProvider
        .addToFavs({ userId: this.user.getUserId(), mapId: this.location._id })
        .subscribe(
          result => {
            this.favs = true;
            this.user.addToFavs(this.location._id);
          },
          error => {
            this.alertCtrl.showError(error.status, error.statusText);
          }
        );
    }
  }

  getTime(distance) {
    return Math.ceil((distance / 40) * 60);
  }

  gotoCommentPage() {
    this.navCtrl.push(CompanyCommentPage, {
      id: this.location._id
    });
  }
}
