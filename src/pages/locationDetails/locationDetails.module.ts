import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { LocationDetailsPage } from "./locationDetails";

@NgModule({
  imports: [IonicPageModule.forChild(LocationDetailsPage)]
})
export class LocationDetailsPageModule {}
