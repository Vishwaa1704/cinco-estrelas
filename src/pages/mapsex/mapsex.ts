import { Component, ElementRef, ViewChild } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps, GoogleMapsAnimation, GoogleMapsEvent, Marker } from '@ionic-native/google-maps';
import { NavController, NavParams, Events } from 'ionic-angular';

import { MarkerProvider } from '../../providers/marker/marker';
import { RestProvider } from '../../providers/rest/rest';
import { UserProvider } from '../../providers/user/user';
import { LeavecommentPage } from '../leavecomment/leavecomment';
import { LocationsListViewPage } from '../locationsListView/locationsListView';
import { FavouritesPage } from '../favourites/favourites';
import { PromotionsPage } from '../promotions/promotions';
import { NotificationPage } from '../notification/notification';
import { MenuPage } from "../menu/menu";
import { CategoriesPage } from "../categories/categories";
import { ActivePagesProvider } from '../../providers/activePages/activePages';
import { AlertProvider } from "../../providers/alert/alert";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: "page-mapsex",
  templateUrl: "mapsex.html"
})
export class MapsexPage {
  @ViewChild('map') mapElement: ElementRef;

  map: any;
  lat;
  lng;
  points;
  markersList: any = [];
  firstTime: any = 0;
  locationPoint;
  markerField = {
    id: '',
    lat: 0,
    lng: 0,
    icon: '',
    title: ''
  };

  home: any;
  favourites: any;
  promotions: any;
  notifications: any;
  showFilters = true;

  constructor(
    public navCtrl: NavController, public navParams: NavParams, private geolocation: Geolocation,
    private restProvider: RestProvider, private alertCtrl: AlertProvider, private user: UserProvider, private markers: MarkerProvider,
    private googleMaps: GoogleMaps, public activePagesProvider: ActivePagesProvider, public translate: TranslateService,
    public events: Events) {
    this.home = LocationsListViewPage;
    this.favourites = FavouritesPage;
    this.promotions = PromotionsPage;
    this.notifications = NotificationPage;
  }

  ionViewWillEnter() {
    if (this.geolocation && this.googleMaps && GoogleMaps.create) {
      this.initializeMap();
    }
    const language = this.user.getSettings().prefferedLanguage;
    this.translate.setDefaultLang(language);
  }

  initializeMap() {
    this.geolocation.getCurrentPosition().then(response => {

      this.lat = response.coords.latitude;
      this.lng = response.coords.longitude;

      // this.setMarkers();

    }).catch(err => this.alertCtrl.showError(500, 'please allow geolocation'));
  }

  setMarkers() {
    this.restProvider.getMapMarkers(
      {
        lat: this.lat,
        lng: this.lng,
        type: this.markers.getCategory(),
        limit: 10,
        radius: 10
      }
    ).subscribe(
      result => {

      },
      err => this.alertCtrl.showError(err.status, err.statusText)
    )

  }

  gotoSettings() {
    this.navCtrl.push(MenuPage);
  }
  gotoCategories() {
    this.navCtrl.push(CategoriesPage);
  }

  setSelectedTab(tab) {
    if (tab == 'home') {
      this.showFilters = this.markers.getShowFilter() || true;
    } else {
      this.showFilters = false;
    }
  }
  gotoMyLocation() {
    this.events.publish('goToMyLocation');
  }
}
