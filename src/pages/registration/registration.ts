import { Component } from "@angular/core";
import { NavController, NavParams, AlertController } from "ionic-angular";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";

import { RegisterOtpPage } from "../../pages/register-otp/register-otp";
import { TermsconditionPage } from "../termscondition/termscondition";
import { RestProvider } from "../../providers/rest/rest";
import { UserProvider } from "../../providers/user/user";
import { HomePage } from "../home/home";

@Component({
  selector: "page-registration",
  templateUrl: "registration.html"
})
export class RegistrationPage {
  private registration: FormGroup;
  public type = "password";
  public showPass = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    private restProvider: RestProvider,
    private user: UserProvider
  ) {
    this.registration = this.formBuilder.group({
      name: ["", Validators.required],
      email: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")
        ])
      ],
      password: [
        "",
        Validators.compose([
          Validators.minLength(5),
          Validators.required,
          Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$") //this is for the letters (both uppercase and lowercase) and numbers validation
        ])
      ]
    });
  }

  ionViewDidLoad() {}
  backToLogin() {
    this.navCtrl.push(HomePage);
  }
  showPassword() {
    this.showPass = !this.showPass;

    if (this.showPass) {
      this.type = "text";
    } else {
      this.type = "password";
    }
  }

  userRegister() {
    // Alert control for displaying error message if otp is invalid
    let alert = this.alertCtrl.create({
      title: "Terms and conditions",
      subTitle: "I have read and accept the",
      buttons: [
        {
          text: "Terms and Conditions.",
          cssClass: "no-style",
          handler: () => this.terms()
        },
        {
          text: "I do not accept",
          role: "cancel",
          cssClass: "cancel",
          handler: () => {}
        },
        {
          text: "I accept",
          cssClass: "accept",
          handler: () => {
            this.acceptConditions();
          }
        }
      ],
      cssClass: "custom-alertBox"
    });
    alert.present();
  }

  terms() {
    this.navCtrl.push(TermsconditionPage);
  }

  showError(code) {
    let alert = this.alertCtrl.create({
      title:
        code && code == 409
          ? "User already exists"
          : "Some error occurred, please try again",
      buttons: [
        {
          text: "OK",
          role: "cancel"
        }
      ],
      cssClass: "custom-alertBox"
    });
    alert.present();
  }

  acceptConditions() {
    this.restProvider.register(this.registration.value).subscribe(
      (result: any) => {
        if (result && result.user) {
          this.user.setUser(result.user);
          this.restProvider.setAuthorization(result.token);
          this.navCtrl.push(RegisterOtpPage);
        } else {
          this.showError(result.code);
        }
      },
      error => {
        this.showError(null);
      }
    );
  }
}
