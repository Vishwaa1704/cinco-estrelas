import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { ConfirmdeletePage } from "../confirmdelete/confirmdelete";

/**
 * Generated class for the CommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 
@Component({
  selector: "page-comment",
  templateUrl: "comment.html"
})
export class CommentPage {
  comment;
  reason;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.reason = this.navParams.get("reason");
  }

  ionViewDidLoad() {
  }
  goToDeleteAccount() {
    this.navCtrl.push(ConfirmdeletePage, { reason: this.reason, comment: this.comment });
  }
}
