import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { LocationsListViewPage } from "./locationsListView";

@NgModule({
  imports: [IonicPageModule.forChild(LocationsListViewPage)]
})
export class MapsexPageModule {}
