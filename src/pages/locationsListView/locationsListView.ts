import { Component, ElementRef, ViewChild } from "@angular/core";
import { Geolocation } from "@ionic-native/geolocation";
import { CategoriesPage } from "../categories/categories";

import { RestProvider } from "../../providers/rest/rest";
import { LocationDetailsPage } from "../locationDetails/locationDetails";
import { MapProvider } from "../../providers/map/map";
import { ToastController, Events, NavController } from "ionic-angular";
import { AlertProvider } from "../../providers/alert/alert";
import { MarkerProvider } from "../../providers/marker/marker";
import { SearchPage } from "../searchPage/searchPage";
import { ActivePagesProvider } from '../../providers/activePages/activePages';
import { GoogleMapsEvent } from '@ionic-native/google-maps';
import _ from 'lodash';

@Component({
  selector: "page-locationsListView",
  templateUrl: "locationsListView.html"
})
export class LocationsListViewPage {
  @ViewChild("map") mapElement: ElementRef;

  locations = [];
  showAsList = false;
  showAsMap = true;
  companies: any = [];
  groupBy = (items, key) =>
    items.reduce(
      (result, item) => ({
        ...result,
        [item[key]]: [...(result[item[key]] || []), item]
      }),
      {}
    );
  keys: any = [];
  lat;
  lng;
  selectedLat;
  selectedLng;
  myLat;
  myLng;
  tab = "loc";
  map: any;
  update = true;

  constructor(
    public navCtrl: NavController,
    private restProvider: RestProvider,
    private alertCtrl: AlertProvider,
    private mapProvider: MapProvider,
    private geolocation: Geolocation,
    private markers: MarkerProvider,
    public toastCtrl: ToastController,
    public activePagesProvider: ActivePagesProvider,
    public events: Events,
  ) {
    this.events.subscribe('markerOpen', () => {
      console.log('coming in marker open');
      this.update = false;
    });
    this.events.subscribe('markerClose', () => {
      console.log('coming in marker close');
      this.update = true;
    });
    this.events.subscribe('goToMyLocation', () => {
      this.initializeMap();
    })
  }

  ionViewWillEnter() {
    this.activePagesProvider.setCurrentPage('listView');

    if (!this.lat && !this.lng) {
      if (this.markers.getSelectedFilter() != "loc") {
        this.getLat(this.markers.getSelectedFilter());
      } else {
        this.tab = "loc";
        this.initializeMap();
      }
    } else {
      this.setMarkers();
    }
  }

  initializeMap() {
    this.geolocation
      .getCurrentPosition()
      .then(response => {
        this.lat = response.coords.latitude;
        this.lng = response.coords.longitude;
        this.myLat = response.coords.latitude;
        this.myLng = response.coords.longitude;
        // this.lat = 37.814346;
        // this.lng = -25.496385;
        this.setMarkers();
        this.mapProvider.setLatLng({ lat: this.lat, lng: this.lng });
        this.markers.setSelectedFilter("loc");
      })
      .catch(error =>
        this.alertCtrl.showError(500, "Please enable geolocation")
      );
  }

  switchToMap() {
    this.showAsMap = true;
    this.showAsList = false;
    this.getLat(this.tab);
  }

  switchToList() {
    this.showAsList = true;
    this.showAsMap = false;
    this.mapProvider.removeMap();
  }

  setMarkers(rad = 30) {
    const latitude = this.lat;
    const longitude = this.lng;
    const category = this.markers.getCategory();
    let radius = rad || 30;
    if (
      this.lat == this.myLat &&
      this.lng == this.myLng &&
      category.indexOf("all") == -1
    ) {
      radius = 3000;
    }
    if (this.lat == 38.468085 && this.lng == -28.359419) {
      radius = 800;
    }
    const toast = this.toastCtrl.create({
      message: "Loading...",
      duration: 3000
    });
    toast.present();
    this.mapProvider.removeMap();

    this.restProvider
      .getMapMarkers({
        lat: latitude,
        lng: longitude,
        type: category,
        limit: 100000,
        radius: radius
      })
      .subscribe(
        (result: any) => {
          this.locations = result;
          let latLngArray = [];
          if (
            this.lat == 42.154058 &&
            this.lng == -8.198415 &&
            result.length == 0
          ) {
            this.lat = 41.14961;
            this.lng = -8.61099;
            this.setMarkers();
          }
          result.forEach(element => {
            latLngArray.push({
              lat: element.location.coordinates[1],
              lng: element.location.coordinates[0]
            });
          });
          this.companies = this.groupBy(result, "locationType");
          this.keys = Object.keys(this.companies);
          if (result.length == 0) {
            const toast = this.toastCtrl.create({
              message: "No results found",
              duration: 3000
            });
            toast.present();
            result = [
              {
                location: {
                  coordinates: [this.lng, this.lat],
                  icon: "red"
                }
              }
            ];
            latLngArray = [{ lat: this.lat, lng: this.lng }];
          }
          if (this.showAsMap) {
            try {
              this.mapProvider.loadMap(result, latLngArray, "map_canvas");
              this.map = this.mapProvider.getMap();
              this.map && this.map.on(GoogleMapsEvent.CAMERA_MOVE_END)
                .subscribe(_.debounce((object) => {
                  if (object[0] && object[0].target) {
                    console.log('coming in change lat', object);
                    const distance = this.getDistance([object[0].southwest.lng, object[0].southwest.lat], {});
                    const move = this.getDistance([object[0].target.lng, object[0].target.lat], {});
                    console.log('coming in move', move, distance, this.update);
                    if (this.update && move > 12) {
                      this.lat = object[0].target.lat;
                      this.lng = object[0].target.lng;
                      // this.map && this.map.remove();
                      this.setMarkers(distance);
                    }
                  }
                }, 1000));
            } catch (e) {
              this.alertCtrl.showError(500, 'please try again');
            }
          }
        },
        err => this.alertCtrl.showError(err.status, err.statusText)
      );
  }

  goToLocationDetailPage(location) {
    this.navCtrl.push(LocationDetailsPage, {
      location
    });
  }

  rad(x) {
    return (x * Math.PI) / 180;
  }

  getDistance(coordinates, location) {
    const p1 = {
      lat: this.lat,
      lng: this.lng
    };
    const p2 = {
      lat: coordinates[1],
      lng: coordinates[0]
    };
    let R = 6378137; // Earth’s mean radius in meter
    let dLat = this.rad(p2.lat - p1.lat);
    let dLong = this.rad(p2.lng - p1.lng);
    let a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.rad(p1.lat)) *
      Math.cos(this.rad(p2.lat)) *
      Math.sin(dLong / 2) *
      Math.sin(dLong / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c * 0.001;
    location["distance"] = Math.ceil(d);
    return Math.ceil(d); // returns the distance in meter
  }

  getLat(dir) {
    let lat = this.lat;
    let lng = this.lng;
    this.tab = dir;
    if (dir == "north") {
      lat = 42.154058;
      lng = -8.198415;
    } else if (dir == "south") {
      lat = 36.960158;
      lng = -7.887096;
    } else if (dir == "center") {
      lat = 38.736946;
      lng = -9.142685;
    } else if (dir == "island") {
      lat = 38.468085;
      lng = -28.359419;
    } else if (dir == "loc") {
      this.initializeMap();
      lat = this.lat;
      lng = this.lng;
    }
    this.lat = lat;
    this.lng = lng;
    this.markers.setSelectedFilter(dir);
    this.setMarkers();
  }

  changeCategory() {
    if (!(this.markers.getCategory().indexOf("all") > -1)) {
      this.markers.setCategory(["all"]);
      this.setMarkers();
    }
  }

  gotoCategories() {
    this.navCtrl.push(CategoriesPage);
  }

  openSearch() {
    this.navCtrl.push(SearchPage);
  }
}
