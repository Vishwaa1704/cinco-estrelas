import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

/*
  Created By: Mohd Sanaullah Ghayasi.
  Dated On: 25-10-2018
*/

@Component({
  selector: "page-termscondition",
  templateUrl: "termscondition.html"
})
export class TermsconditionPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
  }
}
