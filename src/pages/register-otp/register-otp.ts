import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { UserProvider } from '../../providers/user/user';
import { MapsexPage } from '../mapsex/mapsex';
import { WelcomePage } from '../welcome/welcome';
/*
  Created By: Mohd Sanaullah Ghayasi.
  Dated On: 21-10-2018
*/

@Component({
  selector: 'page-register-otp',
  templateUrl: 'register-otp.html',
})
export class RegisterOtpPage {

  otpcode1:any;
  otpcode2:any;
  otpcode3:any;
  otpcode4:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController, private restProvider: RestProvider, private user: UserProvider) {
  }

  ionViewDidLoad() {
  }

  next(el) {
    el.setFocus();
  }

  verifyOtp(){
    let otpCodeStatus = (this.otpcode1 != '' && this.otpcode2 != '') && (this.otpcode3 != '' && this.otpcode4 != '');
    if(!otpCodeStatus) return;

    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      cssClass: 'custom-loader'
    });
  
    loading.present();
    let otp = parseInt(this.otpcode1+this.otpcode2+this.otpcode3+this.otpcode4);
    let email = this.user.getUserEmail();

    this.restProvider.verifyOtp({email, otp}).subscribe(
      (result: any) => {
        // this.user.setUser(result.user);
        loading.dismiss();
        this.navCtrl.setRoot(WelcomePage);
      },
      (error: any) => {
        loading.dismiss();
        this.showError();
      }
    );
  }

  showError() {
    let alert = this.alertCtrl.create({
        title: 'Incorrect code',
        subTitle: 'Please verify and enter the code you have received by email.',
        buttons: [{
          text: 'Try Again',
          role: 'cancel',
          handler: () => {
            this.otpcode1 = '';
            this.otpcode2 = '';
            this.otpcode3 = '';
            this.otpcode4 = '';
          }
        }],
        cssClass: 'custom-alertBox'
      });
      alert.present();
  }

  retryOtp(){
    // Alert control for displaying error message if unable to process otp
    let alert = this.alertCtrl.create({
      title: 'We apologize',
      subTitle: 'We`re having trouble validating your code. Please try again later or contact our service.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Contact',
          handler: () => {
            this.acceptConditions();
          }
        }
      ],
      cssClass: 'custom-alertBox'
    });
    alert.present();
  }
  acceptConditions() {
  }

}
