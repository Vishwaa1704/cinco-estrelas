import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { RecoverypasswordPage } from "../recoverypassword/recoverypassword";
import { RegistrationPage } from "../registration/registration";
import { MapsexPage } from "../mapsex/mapsex";

import { RestProvider } from "../../providers/rest/rest";
import { UserProvider } from "../../providers/user/user";
import { AlertProvider } from "../../providers/alert/alert";
import { NativeStorage } from '@ionic-native/native-storage';

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  credentialsForm: FormGroup;
  visible: boolean = false;
  public type = "password";
  public showPass = false;
  saveData = false;

  constructor(
    public navCtrl: NavController,
    private formBuilder: FormBuilder,
    private restProvider: RestProvider,
    private user: UserProvider,
    private alertCtrl: AlertProvider,
    private nativeStorage: NativeStorage
  ) {
    this.credentialsForm = this.formBuilder.group({
      email: ["", Validators.compose([Validators.required])],
      password: ["", Validators.compose([Validators.required])]
    });
  }
  showPassword() {
    this.showPass = !this.showPass;

    if (this.showPass) {
      this.type = "text";
    } else {
      this.type = "password";
    }
  }

  // This method is for loading recovery page
  loadRecoveryPassword() {
    this.navCtrl.push(RecoverypasswordPage);
  }

  // This method is for loading register page
  loadRegister() {
    this.navCtrl.push(RegistrationPage);
  }
  showHide() {
    this.visible = !this.visible;
  }

  changeSaveData() {
    this.saveData = !this.saveData;
  }

  login() {
    this.restProvider.login(this.credentialsForm.value).subscribe(
      (result: any) => {
        if (result && result.user) {
          this.user.setUser(result.user);
          this.restProvider.setAuthorization(result.token);
          if (this.saveData) {
            this.nativeStorage.setItem('user', result)
              .then(
                () => {
                  this.navCtrl.setRoot(MapsexPage);
                },
                error => console.error('Error storing item', error)
              );
          } else {
            this.navCtrl.setRoot(MapsexPage);
          }
        } else {
          this.alertCtrl.showError(result.code);
        }
      },
      error => {
        this.alertCtrl.showError(error.code, error.message);
      }
    );
  }
}
