import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { AlertProvider } from '../../providers/alert/alert';
import { LocationDetailsPage } from '../locationDetails/locationDetails';
import { MapProvider } from "../../providers/map/map";
import { MarkerProvider } from "../../providers/marker/marker";
import { ToastController } from "ionic-angular";
import { ActivePagesProvider } from '../../providers/activePages/activePages';

@Component({
    selector: "page-searchPage",
    templateUrl: "searchPage.html"
})
export class SearchPage {
    searchText = '';
    locations = [];
    showAsList = false;
    showAsMap = true;
    companies: any = [];
    groupBy = (items, key) =>
        items.reduce(
            (result, item) => ({
                ...result,
                [item[key]]: [...(result[item[key]] || []), item]
            }),
            {}
        );
    keys: any = [];
    lat;
    lng;
    setCategories = false;
    exact = "all";

    constructor(
        public navCtrl: NavController,
        private restProvider: RestProvider,
        private alertCtrl: AlertProvider,
        private mapProvider: MapProvider,
        private markers: MarkerProvider,
        public toastCtrl: ToastController,
        public navParams: NavParams,
        public activePagesProvider: ActivePagesProvider,
        public events: Events,
    ) {
        this.lat = this.mapProvider.getLatLng().lat;
        this.lng = this.mapProvider.getLatLng().lng;
        this.activePagesProvider.setCurrentPage('search');
        this.events.subscribe('reloadSearch', () => {
            console.log('coming in reoad');
            this.exact = "all";
            this.search();
        });
    }

    ionViewDidLoad() {
        this.markers.setShowFilters(true);
    }

    ionViewWillLeave() {
        this.markers.setCategory(["all"]);
    }

    switchToMap() {
        this.showAsMap = true;
        this.showAsList = false;
        this.search();
    }

    switchToList() {
        this.showAsList = true;
        this.showAsMap = false;
    }


    search() {
        let exact = null;
        const text = this.searchText;
        if (this.exact.includes('title')) {
            exact = { "title": { "$regex": text, "$options": "i" } };
        } else if (this.exact.includes('locationType')) {
            exact = { "locationType": { "$regex": text, "$options": "i" } };
        } else if (this.exact.includes('address')) {
            exact = {
                $or: [
                    { "shortAddress": { "$regex": text, "$options": "i" } },
                    { "address": { "$regex": text, "$options": "i" } },
                ]
            };
        } else if (this.exact.includes('email')) {
            exact = { "email": { "$regex": text, "$options": "i" } };
        }
        if (text.length > 0) {
            this.restProvider.searchCompanies({
                text: this.searchText,
                locationType: this.markers.getCategory(),
                exact: exact
            })
                .subscribe(
                    (result: any) => {
                        this.locations = result;
                        let latLngArray = [];
                        result.forEach(element => {
                            latLngArray.push({
                                lat: element.location.coordinates[1],
                                lng: element.location.coordinates[0]
                            })
                        });
                        this.companies = this.groupBy(result, "locationType");
                        this.keys = Object.keys(this.companies);
                        if (this.showAsMap) {
                            try {
                                console.log('coming before loadmap', this.showAsMap, latLngArray);
                                this.mapProvider.loadMap(result, latLngArray, 'map');
                            } catch (e) {
                                this.alertCtrl.showError(500, 'please try again');
                            }
                        }
                    },
                    (err) => this.alertCtrl.showError(err.status, err.statusText)
                )
        }

    }

    goToLocationDetailPage(location) {
        this.navCtrl.push(LocationDetailsPage, {
            location
        })
    }

    rad(x) {
        return (x * Math.PI) / 180;
    }

    getDistance(coordinates, location) {
        const p1 = {
            lat: this.lat,
            lng: this.lng
        };
        const p2 = {
            lat: coordinates[1],
            lng: coordinates[0]
        };
        let R = 6378137; // Earth’s mean radius in meter
        let dLat = this.rad(p2.lat - p1.lat);
        let dLong = this.rad(p2.lng - p1.lng);
        let a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.rad(p1.lat)) *
            Math.cos(this.rad(p2.lat)) *
            Math.sin(dLong / 2) *
            Math.sin(dLong / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = R * c * 0.001;
        location["distance"] = Math.ceil(d);
        return Math.ceil(d); // returns the distance in meter
    }

    backClicked() {
    }

}
