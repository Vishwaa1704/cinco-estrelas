import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { CompanyCommentPage } from "./companyComment";

@NgModule({
  imports: [IonicPageModule.forChild(CompanyCommentPage)]
})
export class CompanyCommentPageModule {}
