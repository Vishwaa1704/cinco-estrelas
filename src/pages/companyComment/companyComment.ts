import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, LoadingController } from "ionic-angular";
import { UserProvider } from '../../providers/user/user';
// import { User } from "firebase";
import { RestProvider } from '../../providers/rest/rest';
import { AlertProvider } from '../../providers/alert/alert';

@Component({
  selector: "page-companyComment",
  templateUrl: "companyComment.html"
})
export class CompanyCommentPage {
  comment: any;
  companyId: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private user: UserProvider,
    private restProvider: RestProvider,
    private alertCtrl: AlertProvider,
    public loadctrl: LoadingController
  ) {
    this.companyId = navParams.get('id');
  }

  submitComment() {
    this.restProvider.addCommentToCompany({
      companyId: this.companyId,
      userId: this.user.getUserId(),
      message: this.comment
    })
      .subscribe(
        (result) => {
          this.alertCtrl.showError(200, 'Submitted');
        },
        (error) => {
          this.alertCtrl.showError(error.code, error.message);
        }
      )
  }
}    