import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { LeavecommentPage } from "./leavecomment";

@NgModule({
 // declarations: [LeavecommentPage],
  imports: [IonicPageModule.forChild(LeavecommentPage)]
})
export class LeavecommentPageModule {}
