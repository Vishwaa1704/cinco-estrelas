import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { FeedbacksuccessPage } from "../feedbacksuccess/feedbacksuccess";
/**
 * Generated class for the LeavecommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 
@Component({
  selector: "page-leavecomment",
  templateUrl: "leavecomment.html"
})
export class LeavecommentPage {
  comment;
  emptyText: boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
  }
  checkChange() {
    if (this.comment.trim().length == 0) {
      this.emptyText = true;
    } else {
      this.emptyText = false;
    }
  }
  sendComment() {
    this.navCtrl.push(FeedbacksuccessPage);
  }
}
