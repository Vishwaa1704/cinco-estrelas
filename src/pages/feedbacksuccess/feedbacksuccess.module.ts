import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { FeedbacksuccessPage } from "./feedbacksuccess";

@NgModule({
//  declarations: [FeedbacksuccessPage],
  imports: [IonicPageModule.forChild(FeedbacksuccessPage)]
})
export class FeedbacksuccessPageModule {}
