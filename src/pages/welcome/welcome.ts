import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { EnableNotificationPage } from "../enable-notification/enable-notification";

/*
  Created By: Mohd Sanaullah Ghayasi.
  Dated On: 25-10-2018
*/

@Component({
  selector: "page-welcome",
  templateUrl: "welcome.html"
})
export class WelcomePage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
  }

  loadWelcome() {
    this.navCtrl.push(EnableNotificationPage);
  }
}
