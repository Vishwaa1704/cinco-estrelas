import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { TermsconditionPage } from "../termscondition/termscondition";
import { WelcomePage } from "../welcome/welcome";

/*
  Created By: Mohd Sanaullah Ghayasi.
  Dated On: 25-10-2018
*/

@Component({
  selector: "page-acceptterms",
  templateUrl: "acceptterms.html"
})
export class AccepttermsPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
  }

  viewDoc() {
    this.navCtrl.push(TermsconditionPage);
  }

  loadWelcome() {
    this.navCtrl.push(WelcomePage);
  }
}
