import { Component } from "@angular/core";
import { NavController, NavParams, LoadingController } from "ionic-angular";
import { UserProvider } from '../../providers/user/user';
// import { User } from "firebase";
import { RestProvider } from '../../providers/rest/rest';
import { AlertProvider } from '../../providers/alert/alert';
import { LocationDetailsPage } from '../locationDetails/locationDetails';
 
@Component({
  selector: "page-notification",
  templateUrl: "notification.html"
})
export class NotificationPage {
  groupBy = (items, key1, key2) => items.reduce(
    (result, item) => ({
      ...result,
      [item[key1][key2]]: [
        ...(result[item[key1][key2]] || []),
        item,
      ],
    }), 
    {},
  );
  notifications:any = [];
  keys = [];
  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private user: UserProvider,
    private restProvider: RestProvider,
    private alertCtrl: AlertProvider,
    public loadctrl: LoadingController
    ) {
    
  }

  ionViewDidEnter() {
    this.loading = this.loadctrl.create({
        spinner: "bubbles"
    });
    this.loading.present();
    this.restProvider.getNotifications({ role: this.user.getUserRole() })
        .subscribe(
            result => {
                this.notifications = result;
                this.loading.dismiss();
            },
            err => {
                this.alertCtrl.showError(err.status, err.statusText);
                this.loading.dismiss();
            }
        )
  }
}