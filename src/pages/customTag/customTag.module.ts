import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { CustomTag } from "./customTag";

@NgModule({
    imports: [IonicPageModule.forChild(CustomTag)]
})
export class CustomTagModule { }
