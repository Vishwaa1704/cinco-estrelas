import { LocationDetailsPage } from "../../pages/locationDetails/locationDetails";
import { App } from 'ionic-angular';
import {
    Component,
} from '@angular/core';

import { HtmlInfoWindow } from "@ionic-native/google-maps";
import { Geolocation } from "@ionic-native/geolocation";

@Component({
    selector: "custom-tag",
    templateUrl: "customTag.html"
})
export class CustomTag {
    position;
    lat;
    lng;
    icon;
    htmInfoWindow: HtmlInfoWindow;

    constructor(
        private geolocation: Geolocation,
        public appCtrl: App
    ) {
    }

    ionViewWillEnter() {
        this.initializeMap();
    }

    onButton_click(event) {
        this.htmInfoWindow.close();
    }

    gotoDetailsPage() {
        const toSend = {
            location: this.position.get('location'),
            _id: this.position.get('_id'),
            icon: this.position.get('icon'),
            title: this.position.get('title'),
            description: this.position.get('description'),
            images: this.position.get('images'),
            locationType: this.position.get('locationType'),
            shortAddress: this.position.get('shortAddress'),
            phone: this.position.get('phone'),
            address: this.position.get('address'),
            email: this.position.get('email'),
            website: this.position.get('website'),
            district: this.position.get('district'),
            obs: this.position.get('obs'),
            availability: this.position.get('availability'),
            ticket: this.position.get('ticket'),
            sent: this.position.get('sent'),
            approved: this.position.get('approved'),
            status: this.position.get('status'),
            comment: this.position.get('comment'),
            hasPromotions: this.position.get('hasPromotions'),
            distance: this.position.get('distance')
        }
        this.appCtrl.getActiveNav().push(LocationDetailsPage, {
            location: toSend
        });
    }

    initializeMap() {
        this.geolocation
            .getCurrentPosition()
            .then(response => {
                this.lat = response.coords.latitude;
                this.lng = response.coords.longitude;
            })
            .catch(error => { });
    }

    rad(x) {
        return (x * Math.PI) / 180;
    }

    getDistance(coordinates, location) {
        const p1 = {
            lat: this.lat,
            lng: this.lng
        };
        const p2 = {
            lat: coordinates[1],
            lng: coordinates[0]
        };
        let R = 6378137; // Earth�s mean radius in meter
        let dLat = this.rad(p2.lat - p1.lat);
        let dLong = this.rad(p2.lng - p1.lng);
        let a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.rad(p1.lat)) *
            Math.cos(this.rad(p2.lat)) *
            Math.sin(dLong / 2) *
            Math.sin(dLong / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = R * c * 0.001;
        location.set("distance", Math.ceil(d));
        return Math.ceil(d); // returns the distance in meter
    }
}
