import { Component } from "@angular/core";
import { NavController, AlertController, LoadingController } from "ionic-angular";
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { UpdateUserByAdminPage } from '../updateUserByAdmin/updateUserByAdmin';

@Component({
  selector: "page-manageusers",
  templateUrl: "manageUsers.html"
})
export class ManageUsersPage {
  users = [];
  private searchUser : FormGroup;

  constructor(
    public navCtrl: NavController,
    private formBuilder: FormBuilder,
    private restProvider: RestProvider,
    private alertCtrl: AlertController,
    public loadctrl: LoadingController,
  ) {
    this.searchUser = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])],
    })
  }

  ngOnInit() {
    this.getUsers({ page: 1, perPage: 10 });
  }

  showError() {
    let alert = this.alertCtrl.create({
      title: 'Some error occurred, please try again',
      buttons: [
        {
          text: 'OK',
          role: 'cancel'
        }
      ],
      cssClass: 'custom-alertBox'
    });
    alert.present();
  }

  getUsers(options) {
    this.restProvider.getUsersList(options).subscribe(
      (result:any) => (this.users = result),
      (err) => this.showError()
    )
  }

  viewUser(user) {
    this.navCtrl.push(UpdateUserByAdminPage, user);
  }

  search() {
    if(this.searchUser.value.email.length == 0) {
      this.getUsers({ page: 1, perPage: 10 });
    } else {
      this.getUsers(this.searchUser.value);      
    }
  }
}