import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ManageUsersPage } from "./manageUsers";

@NgModule({
  imports: [IonicPageModule.forChild(ManageUsersPage)]
})
export class ManageUsersPageModule {}
