import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { RestProvider } from '../../providers/rest/rest';
import { UserProvider } from '../../providers/user/user';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the LanguagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: "page-language",
  templateUrl: "language.html"
})
export class LanguagePage {
  language;
  settings;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    private restProvider: RestProvider,
    private userProvider: UserProvider,
    private alertCtrl: AlertController,
    public loadctrl: LoadingController,
    public translate: TranslateService
  ) {
    this.settings = this.userProvider.getSettings();
    this.language = this.settings.prefferedLanguage;
    this.storage.set("language", this.language);
    // this.storage.get("language").then(data => {
    //   this.language = data;
    // });
  }

  ionViewDidLoad() {
  }

  showError(code, text) {
    let alert = this.alertCtrl.create({
      title: code && code == 409 ? 'User already exists' : text || 'Some error occurred, please try again',
      buttons: [
        {
          text: 'OK',
          role: 'cancel'
        }
      ],
      cssClass: 'custom-alertBox'
    });
    alert.present();
  }

  languageChange() {
    let loading = this.loadctrl.create({
      spinner: "bubbles"
    });
    loading.present();
    this.restProvider.updateProfile({ prefferedLanguage: this.language }, this.settings.id).subscribe(
      (result) => {
        loading.dismiss();
        this.userProvider.setSettings(result);
        this.storage.set("language", this.language);
        this.translate.setDefaultLang(this.language);
        this.showError(200, 'updated sucussefully');
      },
      (error) => {
        loading.dismiss();
        this.showError(500, null);
      }
    )
  }
}
