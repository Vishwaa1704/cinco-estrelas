import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ItemSliding
} from "ionic-angular";
import { UserProvider } from "../../providers/user/user";
// import { User } from "firebase";
import { RestProvider } from "../../providers/rest/rest";
import { AlertProvider } from "../../providers/alert/alert";
import { LocationDetailsPage } from "../locationDetails/locationDetails";

import * as _ from "lodash";

@Component({
  selector: "page-promotions",
  templateUrl: "promotions.html"
})
export class PromotionsPage {
  groupBy = (items, key1, key2) =>
    items.reduce(
      (result, item) => ({
        ...result,
        [item[key1][key2]]: [...(result[item[key1][key2]] || []), item]
      }),
      {}
    );
  promotions = {};
  keys = [];
  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private user: UserProvider,
    private restProvider: RestProvider,
    private alertCtrl: AlertProvider,
    public loadctrl: LoadingController
  ) { }

  ionViewDidEnter() {
    this.loading = this.loadctrl.create({
      spinner: "bubbles"
    });
    this.loading.present();
    this.restProvider.getPromotions().subscribe(
      result => {
        try {
          this.promotions = this.groupBy(result, "locationId", "locationType");
          this.keys = Object.keys(this.promotions);
          this.loading.dismiss();
        } catch {
          this.alertCtrl.showError(500, "some error occurred");
          this.loading.dismiss();
        }
      },
      err => {
        this.alertCtrl.showError(err.status, err.statusText);
        this.loading.dismiss();
      }
    );
  }

  ionViewDidLoad() { }
  deleteItem(item: ItemSliding, locationId, key) {
    let val = _.find(this.promotions[key], { _id: locationId });
    this.promotions[key].splice(val, 1);
  }

  goToLocationDetailPage(location) {
    this.navCtrl.push(LocationDetailsPage, {
      location
    });
  }
}
