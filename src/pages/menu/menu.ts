import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController
} from "ionic-angular";
import { ProfilePage } from "../profile/profile";
import { FavouritesPage } from "../favourites/favourites";
import { LanguagePage } from "../language/language";
import { AddLocationsPage } from "../addLocations/addLocations";
import { ManageUsersPage } from "../manageUsers/manageUsers";

import { Storage } from "@ionic/storage";
import { ChangepasswordPage } from "../changepassword/changepassword";
import { CancelaccountPage } from "../cancelaccount/cancelaccount";
import { LeavecommentPage } from "../leavecomment/leavecomment";
import { UserProvider } from "../../providers/user/user";
import { RestProvider } from "../../providers/rest/rest";
import { SocialSharing } from "@ionic-native/social-sharing";
import { Platform } from "ionic-angular";
import { SlidesPage } from '../../app/slides/slides';
/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-menu",
  templateUrl: "menu.html"
})
export class MenuPage {
  language;
  settings;
  touchIdEnable = false;
  locationEnabled = true;
  notificationsEnabled = false;
  role = "user";
  iosAppLink: any;
  appShareMessage: string;
  appShareSubject: string;
  filePath: string;
  androidAppLink: any;
  AppLink: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    private userProvider: UserProvider,
    private restProvider: RestProvider,
    public loadctrl: LoadingController,
    private alertCtrl: AlertController,
    private socialSharing: SocialSharing,
    public plt: Platform
  ) { }

  ionViewDidLoad() { }
  goToProfile() {
    this.navCtrl.push(ProfilePage);
  }
  goToFavourites() {
    this.navCtrl.push(FavouritesPage);
  }
  goToLanguage() {
    this.navCtrl.push(LanguagePage);
  }
  ionViewWillEnter() {
    this.settings = this.userProvider.getSettings();
    this.language = this.settings.prefferedLanguage;
    // this.touchIdEnable = this.settings.touchIdEnable;
    this.locationEnabled = this.settings.locationEnabled;
    this.notificationsEnabled = this.settings.notificationsEnabled;
    this.role = this.settings.role;
    // this.storage.get("language").then(data => {
    //   this.language = data;
    // });
  }
  changePassword() {
    this.navCtrl.push(ChangepasswordPage);
  }
  goToCancel() {
    this.navCtrl.push(CancelaccountPage);
  }
  goToComment() {
    this.navCtrl.push(LeavecommentPage);
  }
  shareApp() {
    if (this.plt.is("ios")) {
      this.AppLink = this.userProvider.getSettings().iosAppStoreLink;
    } else if (this.plt.is("android")) {
      this.AppLink = this.userProvider.getSettings().androidPlayStoreLink;
    } else {
      this.AppLink = this.settings.androidAppLink;
    }
    this.appShareMessage = "Download the Cinco App app here";
    this.appShareSubject = "Cinco Estrelas";
    this.filePath = "";
    this.socialSharing
      .share(
        this.appShareMessage,
        this.appShareSubject,
        this.filePath,
        this.AppLink
      )
      .then(
        () => {
        },
        err => {
        }
      );
  }
  showError(code, text) {
    let alert = this.alertCtrl.create({
      title:
        code && code == 409
          ? "User already exists"
          : text || "Some error occurred, please try again",
      buttons: [
        {
          text: "OK",
          role: "cancel"
        }
      ],
      cssClass: "custom-alertBox"
    });
    alert.present();
  }

  updateSetting(setting) {
    this[setting] = !this[setting];
    let loading = this.loadctrl.create({
      spinner: "bubbles"
    });
    loading.present();
    const update = {};
    update[setting] = this[setting];
    this.restProvider.updateProfile(update, this.settings.id).subscribe(
      result => {
        loading.dismiss();
        this.userProvider.setSettings(result);
        this.showError(200, "updated sucussefully");
      },
      error => {
        loading.dismiss();
        this.showError(500, null);
      }
    );
  }

  openAddLocationsPage() {
    this.navCtrl.push(AddLocationsPage);
  }

  goToManageUserPage() {
    this.navCtrl.push(ManageUsersPage);
  }

  logout() {
    this.userProvider.logout();
    this.navCtrl.setRoot(SlidesPage);
  }
}
