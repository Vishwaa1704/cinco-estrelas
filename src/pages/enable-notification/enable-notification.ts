import { Component } from "@angular/core";
import { NavController, NavParams, AlertController } from "ionic-angular";
import { EnableLocalizationPage } from "../enable-localization/enable-localization";

/*
  Created By: Mohd Sanaullah Ghayasi.
  Dated On: 25-10-2018
*/

@Component({
  selector: "page-enable-notification",
  templateUrl: "enable-notification.html"
})
export class EnableNotificationPage {
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {}

  ionViewDidLoad() {
  }

  enableNotification() {
    // Alert control for displaying
    let alert = this.alertCtrl.create({
      title: '“5 Estrelas” gostava de lhe enviar notificações',
      subTitle: "As notificações podem ser alertas, sonse ícones, e podem ser configuradas nas Definições.",
      buttons: [
        {
          text: "Não Permitir",
          role: "cancel",
          handler: () => {
          }
        },
        {
          text: "Permitir",
          handler: () => {
            this.turnOnMove();
          }
        }
      ],
      cssClass: "custom-alertBox"
    });
    alert.present();
  }
  turnOnMove() {
    this.navCtrl.push(EnableLocalizationPage);
  }
}
