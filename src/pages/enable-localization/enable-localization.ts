import { Component } from "@angular/core";
import { NavController, NavParams, AlertController } from "ionic-angular";
import { MapsexPage } from '../mapsex/mapsex';
/*
  Created By: Mohd Sanaullah Ghayasi.
  Dated On: 25-10-2018
*/

@Component({
  selector: "page-enable-localization",
  templateUrl: "enable-localization.html"
})
export class EnableLocalizationPage {
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {}

  ionViewDidLoad() {
  }

  autorizeLoaclization() {
    // Alert control for displaying
    let alert = this.alertCtrl.create({
      title: 'Permite que “5 Estrelas” aceda à sua localização enquanto usa a app?',
      subTitle: "A sua localização será utilizada para lhe apresentar sugestões Cinco Estrelas perto de si e para calcular distâncias e percursos.",
      buttons: [
        {
          text: "Não Permitir",
          role: "cancel",
          handler: () => {
          }
        },
        {
          text: "Permitir",
          handler: () => {
            this.moveToMapPage()
          }
        }
      ],
      cssClass: "custom-alertBox"
    });
    alert.present();
  }

  moveToMapPage() {
    this.navCtrl.push(MapsexPage);
  }
}
