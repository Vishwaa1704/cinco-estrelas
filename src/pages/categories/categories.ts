import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, Events } from "ionic-angular";
import { MenuPage } from "../menu/menu";
import { MarkerProvider } from '../../providers/marker/marker';
import { MapsexPage } from '../mapsex/mapsex';
import { SearchPage } from '../searchPage/searchPage';
import { ActivePagesProvider } from '../../providers/activePages/activePages';

/**
 * Generated class for the CategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: "page-categories",
  templateUrl: "categories.html"
})
export class CategoriesPage {
  previousCls: any = "";
  categories = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private markers: MarkerProvider,
    public activePagesProvider: ActivePagesProvider,
    public events: Events,
  ) {
    this.categories = this.markers.getCategory();
  }

  ionViewDidLoad() {
  }
  gotoMenu() {
    this.navCtrl.push(MenuPage);
  }
  goBack() {
    this.navCtrl.pop();
  }

  setMapCategory(category) {
    if (this.categories && this.categories.length > 0 && this.categories.indexOf(category) > -1) {
      const index = this.categories.indexOf(category);
      this.categories.splice(index, 1);
      if (this.categories.length == 0) {
        this.categories.push('all');
      }
    } else {
      if (this.categories && this.categories.length > 0 && this.categories.indexOf('all') > -1) {
        const index = this.categories.indexOf('all');
        this.categories.splice(index, 1);
      }
      this.categories.push(category);
    }

  }

  submit() {
    this.markers.setCategory(this.categories);
    if (this.activePagesProvider.getCurrentPage().includes('search')) {
      this.events.publish('reloadSearch');
      this.navCtrl.popTo(SearchPage, { updateUrl: true });
    } else {
      this.navCtrl.setRoot(MapsexPage);
    }
  }

  includeCategory(category) {
    return this.categories && this.categories.length > 0 && this.categories.indexOf(category) > -1;
  }
}
