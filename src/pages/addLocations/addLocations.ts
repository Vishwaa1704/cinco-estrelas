import { Component } from "@angular/core";
import { NavController, AlertController, LoadingController } from "ionic-angular";
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';

@Component({
  selector: "page-locations",
  templateUrl: "addLocations.html"
})
export class AddLocationsPage {
  private locations: FormGroup;

  constructor(
    public navCtrl: NavController,
    private formBuilder: FormBuilder,
    private restProvider: RestProvider,
    private alertCtrl: AlertController,
    public loadctrl: LoadingController,
  ) {

    this.locations = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      type: ['', Validators.required],
      latitude: [0, Validators.required],
      longitude: [0, Validators.required],
      icon: [''],
      shortAddress: [''],
      image1: [''],
      image2: [''],
      image3: [''],
      phone: [''],
      address: [''],
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])],
      website: ['']
    });

  }

  showError(code, text) {
    let alert = this.alertCtrl.create({
      title: code && code == 409 ? 'User already exists' : text || 'Some error occurred, please try again',
      buttons: [
        {
          text: 'OK',
          role: 'cancel'
        }
      ],
      cssClass: 'custom-alertBox'
    });
    alert.present();
  }

  addNewLocation() {
    const toAdd = {
      "latitude": this.locations.value.latitude,
      "longitude": this.locations.value.longitude,
      "icon" : this.locations.value.icon || 'green',
      "title" : this.locations.value.title,
      "locationType" : this.locations.value.type,
      "description": this.locations.value.description,
      "shortAddress": this.locations.value.shortAddress || '',
      "images": [this.locations.value.image1, this.locations.value.image2, this.locations.value.image3],
      "phone": this.locations.value.phone,
      "address": this.locations.value.address,
      "email": this.locations.value.email,
      "website": this.locations.value.website
    }
    let loading = this.loadctrl.create({
      spinner: "bubbles"
    });
    loading.present();

    this.restProvider.addNewLocation(toAdd).subscribe(
      (result) => {
        loading.dismiss();
        this.showError(200, 'updated sucussefully');
      },
      (error) => {
        loading.dismiss();
        this.showError(500, null);
      }
    )
  }
}
