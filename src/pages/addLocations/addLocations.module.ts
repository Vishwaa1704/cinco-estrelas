import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AddLocationsPage } from "./addLocations";

@NgModule({
  imports: [IonicPageModule.forChild(AddLocationsPage)]
})
export class AddLocationsPageModule {}
