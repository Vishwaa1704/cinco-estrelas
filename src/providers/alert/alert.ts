import { Injectable } from '@angular/core';
import { AlertController } from "ionic-angular";
import * as httpStatus from "http-status";

@Injectable()
export class AlertProvider {
  category = 'all';

  constructor(private alertCtrl: AlertController) {
  }

  showError(code=500, message='') {
    let title = message;
    if(!message || (message && message.length == 0)) {
        title = httpStatus[code];
    }

    let alert = this.alertCtrl.create({
        title: title,
        buttons: [
          {
            text: 'OK',
            role: 'cancel'
          }
        ],
        cssClass: 'custom-alertBox'
      });
      alert.present();
  }

}
