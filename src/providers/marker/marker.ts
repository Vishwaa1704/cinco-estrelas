import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the MarkerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MarkerProvider {
  category = ['all'];
  selectedFilter = 'loc';
  showFilters = true;

  constructor(public http: HttpClient) {
  }

  setCategory(c) {
    console.log('coming in set category', c);
    this.category = c;
  }

  getCategory() {
    console.log('coming in get category', this.category)
    return this.category;
  }

  setSelectedFilter(filter) {
    this.selectedFilter = filter;
  }

  getSelectedFilter() {
    return this.selectedFilter;
  }

  setShowFilters(filter) {
    this.showFilters = filter;
  }

  getShowFilter() {
    return this.showFilters;
  }

}
