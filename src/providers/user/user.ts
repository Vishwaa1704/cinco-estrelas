import { Injectable } from '@angular/core';

@Injectable()
export class UserProvider {
  id: string;
  role: string;
  email: string;
  name: string;
  picture = null;
  favourites: any;
  prefferedLanguage: string = 'en';
  touchIdEnable: boolean = false;
  locationEnabled: boolean = false;
  notificationsEnabled: boolean = false;

  constructor() { }

  setUser(user) {
    this.id = user.id;
    this.role = user.role;
    this.email = user.email;
    this.name = user.name;
    this.picture = user.picture;
    this.favourites = user.favourites;
    this.prefferedLanguage = user.prefferedLanguage;
    this.touchIdEnable = user.touchIdEnable;
    this.locationEnabled = user.locationEnabled;
    this.notificationsEnabled = user.notificationsEnabled;
  }

  getUserRole() {
    return this.role;
  }

  getUserEmail() {
    return this.email;
  }

  getUserId() {
    return this.id;
  }

  getProfileImage() {
    return this.picture;
  }

  getSettings() {
    return {
      favourites: this.favourites,
      prefferedLanguage: this.prefferedLanguage,
      touchIdEnable: this.touchIdEnable,
      locationEnabled: this.locationEnabled,
      notificationsEnabled: this.notificationsEnabled,
      name: this.name,
      email: this.email,
      picture: this.picture,
      id: this.id,
      role: this.role,
      iosAppStoreLink: "https://itunes.apple.com/in/app/whatsapp-messenger/id310633997?mt=8",
      androidPlayStoreLink: "https://play.google.com/store/apps/details?id=com.whatsapp"
    }
  }

  setSettings(obj) {
    Object.keys(obj).map((keys) => {
      this[keys] = obj[keys];
    })
  }

  addToFavs(id) {
    this.favourites.push(id);
  }

  removeFromFavs(id) {
    const index = this.favourites.indexOf(id);
    if (index > -1) {
      this.favourites.splice(index, 1);
    }
  }

  logout() {
    this.id = null;
    this.role = null;
    this.email = null;
    this.name = null;
    this.picture = null;
    this.favourites = null;
    this.prefferedLanguage = null;
    this.touchIdEnable = null;
    this.locationEnabled = null;
    this.notificationsEnabled = null;
  }
}