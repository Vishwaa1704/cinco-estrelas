import { Platform, LoadingController, Events } from 'ionic-angular';
import { ToastController } from "ionic-angular";

import {
    ComponentRef,
    Injector,
    ApplicationRef,
    ComponentFactoryResolver,
    NgZone,
    Injectable
} from '@angular/core';

import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    GoogleMapOptions,
    BaseArrayClass,
    MarkerOptions,
    Marker,
    HtmlInfoWindow,
    GoogleMapsAnimation,
    Environment
} from '@ionic-native/google-maps';
import { CustomTag } from '../../pages/customTag/customTag';

@Injectable()
export class MapProvider {
    lat: null;
    lng: null;
    map: GoogleMap;
    htmInfoWindow: HtmlInfoWindow;
    icon;
    showAsMap = true;
    showAsList = false;

    constructor(
        public platform: Platform,
        private injector: Injector,
        private resolver: ComponentFactoryResolver,
        private appRef: ApplicationRef,
        private _ngZone: NgZone,
        public loadctrl: LoadingController,
        public toastCtrl: ToastController,
        public events: Events,
    ) { }

    ionViewDidLoad() {
        Environment.setEnv({
            'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyA9abaBFOYID1WWgqXv6gxn5zdfIz4KM4w',
            'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyA9abaBFOYID1WWgqXv6gxn5zdfIz4KM4w'
        });

    }

    setLatLng(obj) {
        this.lat = obj.lat;
        this.lng = obj.lng;
    }

    getLatLng() {
        return { lat: this.lat, lng: this.lng };
    }

    switchToMap() {
        this.showAsMap = true;
        this.showAsList = false;
    }

    switchToList() {
        this.showAsList = true;
        this.showAsMap = false;
    }

    getListMap() {
        return {
            list: this.showAsList,
            map: this.showAsMap
        }
    }

    removeMap() {
        this.map && this.map.remove();
    }

    loadMap(locations: MarkerOptions, targetArray, cls) {
        console.log('coming in load map', this.map);
        this.map && this.map.remove();
        let mapOptions: GoogleMapOptions = {
            camera: {
                target: targetArray,
                padding: 60,
                tilt: 90,
                zoom: 10
            }
        };
        this.map = GoogleMaps.create(cls, mapOptions);
        this.map.one(GoogleMapsEvent.MAP_READY)
            .then(() => {
                // loading.dismiss();
                this.htmInfoWindow = new HtmlInfoWindow();

                // Create markers
                let mvcArray: BaseArrayClass<MarkerOptions> = new BaseArrayClass<MarkerOptions>(locations);
                mvcArray.mapAsync((markerOpts: MarkerOptions, next: (marker: Marker) => void) => {
                    this.map.addMarker(markerOpts).then(next);
                }).then((markers: Marker[]) => {

                    // Listen the MARKER_CLICK event on all markers
                    markers.forEach((marker: Marker) => {
                        const m = this.setMarkerIcon(marker);
                        m.on(GoogleMapsEvent.MARKER_CLICK).subscribe((parmas: any[]) => {
                            this.onMarkerClick(parmas, marker);
                        });
                    });
                });

            });
    }

    setMarkerIcon(marker) {
        let icon = marker.get('locationType')
            ? `assets/imgs/categories/${marker.get('locationType')
                .toLowerCase()
                .trim()
                .split(" ")
                .join("_")}/${marker.get('hasPromotions') ? "promo" : "map"}.png`
            : "green";
        marker.set('icon', icon);
        let m = null;
        try {
            m = this.map.addMarkerSync({
                position: {
                    lat: marker.get('location').coordinates[1],
                    lng: marker.get('location').coordinates[0]
                },
                icon: icon,
                animation: GoogleMapsAnimation.BOUNCE
            });
        } catch (err) {
            try {
                m = this.map.addMarkerSync({
                    position: {
                        lat: marker.get('location').coordinates[1],
                        lng: marker.get('location').coordinates[0]
                    },
                    icon: 'green',
                    animation: GoogleMapsAnimation.BOUNCE
                });
            } catch (e) {
            }
        }
        return m;
    }

    onMarkerClick(params: any[], m) {
        // Get a marker instance from the passed parameters
        let marker: Marker = params.pop();

        // Create a component
        const compFactory = this.resolver.resolveComponentFactory(CustomTag);
        let compRef: ComponentRef<CustomTag> = compFactory.create(this.injector);
        compRef.instance.position = m;
        compRef.instance.icon = m.get('icon');
        compRef.instance.htmInfoWindow = this.htmInfoWindow;

        this.appRef.attachView(compRef.hostView);

        let div = document.createElement('div');
        div.appendChild(compRef.location.nativeElement);

        // Dynamic rendering
        this._ngZone.run(() => {
            this.htmInfoWindow.setContent(div, {
                width: "230px",
                height: "40px"
            });
            this.events.publish('markerOpen');
            this.htmInfoWindow.open(marker);
        });

        // Destroy the component when the htmlInfoWindow is closed.
        this.htmInfoWindow.one(GoogleMapsEvent.INFO_CLOSE).then(() => {
            console.log('coming >>>>>>');
            this.events.publish('markerClose');
            compRef.destroy();
        });
    }

    getMap() {
        return this.map;
    }
}