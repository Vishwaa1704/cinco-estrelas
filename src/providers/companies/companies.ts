import { Injectable } from '@angular/core';

@Injectable()
export class CompanyProviders {
    locations = [];
    companies = [];
    keys = [];
    groupBy = (items, key) =>
        items.reduce(
            (result, item) => ({
                ...result,
                [item[key]]: [...(result[item[key]] || []), item]
            }),
            {}
        );
    latLngArray = [];

    setCompanies(result) {
        this.locations = result;
        result.forEach(element => {
            this.latLngArray.push({
                lat: element.location.coordinates[1],
                lng: element.location.coordinates[0]
            });
        });
        this.companies = this.groupBy(result, "locationType");
        this.keys = Object.keys(this.companies);
    }

    getLocations() {
        return this.locations;
    }

    getCompanies() {
        return this.companies;
    }

    getKeys() {
        return this.keys;
    }
}