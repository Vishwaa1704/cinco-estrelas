import { Injectable } from '@angular/core';

@Injectable()
export class ActivePagesProvider {
    currentPage = '';
    constructor() {
    }

    setCurrentPage(page) {
        this.currentPage = page;
    }

    getCurrentPage() {
        return this.currentPage;
    }
}