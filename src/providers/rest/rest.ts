import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import urls from '../../configs/urls';


@Injectable()
export class RestProvider {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private httpClient : HttpClient) { 
  }

  jsonToParams(option) {
    let urlParameters = Object.keys(option).map(function(k) {
        return encodeURIComponent(k) + '=' + encodeURIComponent(option[k])
    }).join('&');

    return urlParameters;
  }

  register(fields) {
    const formFields = JSON.stringify(fields);
    return this.httpClient.post(`${urls.baseUrl}${urls.register}`, formFields, this.httpOptions);
  }
  
  login(userId) {
    const formFields = JSON.stringify(userId);
    return this.httpClient.post(`${urls.baseUrl}${urls.login}`, formFields, this.httpOptions); 
  }

  setAuthorization(token) {
    this.httpOptions.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${token.tokenType} ${token.accessToken}`
    })
    // this.httpOptions.headers.append("Authorization", token.tokenType + token.accessToken );
  }

  verifyOtp(otp) {
    const formFields = JSON.stringify(otp);
    return this.httpClient.post(`${urls.baseUrl}${urls.verifyOtp}`, formFields, this.httpOptions);
  }

  getAccount(userId) {
    return this.httpClient.get(`${urls.baseUrl}${urls.getAccount}`, this.httpOptions);
  }

  getNotifications(option) {
    const formFields = JSON.stringify(option);
    return this.httpClient.post(`${urls.baseUrl}${urls.getNotifications}`, formFields, this.httpOptions);
  }

  getProfile() {
    return this.httpClient.get(`${urls.baseUrl}${urls.getProfile}`);
  }

  modifyProfile(profile) {
    const formFields = JSON.stringify(profile);
    return this.httpClient.post(`${urls.baseUrl}${urls.modifyProfile}`, formFields, this.httpOptions);
  }

  setPushNotificationStatus(status) {
    const formFields = JSON.stringify(status);
    return this.httpClient.post(`${urls.baseUrl}${urls.setPushNotificationStatus}`, formFields, this.httpOptions);
  }

  setEmailNotificationStatus(status) {
    const formFields = JSON.stringify(status);
    return this.httpClient.post(`${urls.baseUrl}${urls.setEmailNotificationStatus}`, formFields, this.httpOptions);
  }

  getMapMarkers(latLng) {
    const formFields = JSON.stringify(latLng);
    return this.httpClient.post(`${urls.baseUrl}${urls.getMapMarkers}`, formFields, this.httpOptions);
  }

  updateProfile(updates, id) {
    const formFields = JSON.stringify(updates);
    return this.httpClient.patch(`${urls.baseUrl}${urls.updateProfile}/${id}`, formFields, this.httpOptions);
  }

  addNewLocation(location) {
    const formFields = JSON.stringify(location);
    return this.httpClient.post(`${urls.baseUrl}${urls.addNewLocation}`, formFields, this.httpOptions);
  }

  getUsersList(option) {
    return this.httpClient.get(`${urls.baseUrl}${urls.getUsersList}?${this.jsonToParams(option)}`, this.httpOptions);
  }

  updateProfileByAdmin(updates, id) {
    const formFields = JSON.stringify(updates);
    return this.httpClient.patch(`${urls.baseUrl}${urls.getUsersList}/${id}`, formFields, this.httpOptions);
  }

  addToFavs(option) {
    const formFields = JSON.stringify(option);
    return this.httpClient.post(`${urls.baseUrl}${urls.addFavs}`, formFields, this.httpOptions);
  }

  removeFromFavs(option) {
    const formFields = JSON.stringify(option);
    return this.httpClient.post(`${urls.baseUrl}${urls.removeFavs}`, formFields, this.httpOptions);
  }

  getFavs(option) {
    const formFields = JSON.stringify(option);
    return this.httpClient.post(`${urls.baseUrl}${urls.getFavs}`, formFields, this.httpOptions);
  }

  getCoordinates(dir) {
    const formFields = JSON.stringify(dir);
    return this.httpClient.post(`${urls.baseUrl}${urls.getCoordinates}`, formFields, this.httpOptions);
  }

  searchCompanies(option) {
    console.log('options', option);
    const formFields = JSON.stringify(option);
    return this.httpClient.post(`${urls.baseUrl}${urls.searchCompanies}`, formFields, this.httpOptions);
  }

  cancelAccount(option) {
    const formFields = JSON.stringify(option);
    return this.httpClient.post(`${urls.baseUrl}${urls.cancelAccount}`, formFields, this.httpOptions);
  }

  getPromotions() {
    return this.httpClient.get(`${urls.baseUrl}${urls.getPromotions}`, this.httpOptions);
  }

  addCommentToCompany(option) {
    const formFields = JSON.stringify(option);
    return this.httpClient.post(`${urls.baseUrl}${urls.addCommentToCompany}`, formFields, this.httpOptions);
  }
}