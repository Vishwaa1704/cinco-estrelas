const urls = {
  baseUrl: "http://18.191.189.162:3000/v1",
  // "baseUrl": "http://localhost:3000/v1",
  register: "/auth/register",
  login: "/auth/login",
  verifyOtp: "/verify/otp",
  getAccount: "/user/getAccount",
  getMapMarkers: "/map/getMapMarkers",
  getAllLocations: "/getTransactions",
  getNotifications: "/notification/getAll",
  getProfile: "/getProfile",
  modifyProfile: "/modifyProfile",
  setPushNotificationStatus: "/setPushNotificationStatus",
  setEmailNotificationStatus: "/setEmailNotificationStatus",
  updateProfile: "/users",
  addNewLocation: "/map/addNewLocation",
  getUsersList: "/users",
  addFavs: "/users/addToFavs",
  removeFavs: "/users/removeFromFavs",
  getFavs: "/map/findMultiple",
  getCoordinates: "/map/getCoordinates",
  searchCompanies: "/map/search",
  cancelAccount: "/user/cancelAccount",
  getPromotions: "/map/getPromotions",
  addCommentToCompany: "/map/addCommentToCompany"
};

export default urls;
