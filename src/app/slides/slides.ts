import { Component } from "@angular/core";
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook";
import { GooglePlus } from "@ionic-native/google-plus";
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  AlertController
} from "ionic-angular";

import { HomePage } from "../../pages/home/home";
import { MapsexPage } from "../../pages/mapsex/mapsex";
import { RegistrationPage } from "../../pages/registration/registration";
import { TouchID } from "@ionic-native/touch-id";
import { AlertProvider } from "../../providers/alert/alert";
import { NativeStorage } from '@ionic-native/native-storage';
import { RestProvider } from "../../providers/rest/rest";
import { UserProvider } from "../../providers/user/user";

//import { TranslateModule } from '@ngx-translate/core';

/**
 * Generated class for the SlidesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-slides",
  templateUrl: "slides.html"
})
export class SlidesPage {
  //slides:[];
  dir: string = "ltr";
  userData: { email: any; first_name: any; picture: any; username: any };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public afauth: AngularFireAuth,
    public gplus: GooglePlus,
    private fb: Facebook,
    private touchId: TouchID,
    public alertCtrl: AlertProvider,
    private nativeStorage: NativeStorage,
    private restProvider: RestProvider,
    private user: UserProvider,
  ) {
    this.dir = platform.dir();
  }

  ionViewWillLoad() {
    console.log('****');
    this.nativeStorage.getItem('user')
      .then(
        (result) => {
          console.log('****', result);
          if (result && result.token && new Date(result.token.expiresIn) > new Date()) {
            console.log('***** inside if');
            this.user.setUser(result.user);
            this.restProvider.setAuthorization(result.token);
            this.navCtrl.setRoot(MapsexPage);
          }
        },
        error => console.error('Error getting item', error)
      );
  }

  loginWithFb() {
    this.fb.login(["public_profile", "email"]).then(
      (res: FacebookLoginResponse) => {
        this.fb
          .api("me?fields=id,name,email,first_name,picture", [
            "public_profile",
            "email"
          ])
          .then(profile => {
            this.userData = {
              email: profile["email"],
              first_name: profile["first_name"],
              picture: profile["picture_large"]["data"]["url"],
              username: profile["name"]
            };
            this.navCtrl.push(MapsexPage);
          });
      },
      error => { }
    );
  }

  // This method is used for navigating to home page for login with Email
  loginWithEmail() {
    this.navCtrl.push(HomePage);
  }

  createAccount() {
    this.navCtrl.push(RegistrationPage);
  }
  googleLogin() {
    // alert('iam here');
    console.log('google login');
    this.gplus
      .login({
        webClientId:
          "349137692766-r4jgv6csksdkb641t8ud07jn9slts8no.apps.googleusercontent.com",
        offline: true
      })
      .then(
        data => {
          console.log('data came', data);
          firebase
            .auth()
            .signInWithCredential(
              firebase.auth.GoogleAuthProvider.credential(data.idToken)
            )
            .then(
              data2 => {
                //alert("login suc");
                this.navCtrl.push(MapsexPage);
              },
              error => {
                // alert("error in credential");
              }
            );
        },
        err => {
          alert("err in gplus");
        }
      );
  }
  loginTouchID() {
    this.touchId.verifyFingerprint("Scan your fingerprint please").then(
      res => {
        this.navCtrl.setRoot(MapsexPage);
        var userTouchID = res;
        console.log("Response", res);
      },
      err => {
        this.alertCtrl.showError(0, "TouchID is not available");
        console.log("error", err);
      }
    );
  }
  //  googleLogin(){
  //   this.gplus.login({
  //     'webClientId':'140635668585-42ph815ot6sg8b5frdbpnvo5vnu7bdmi.apps.googleusercontent.com',
  //     'offline':true
  //   }).then(data=>{
  //     console.log('data',data);
  //     firebase.auth().signInWithCredential(firebase.auth.GoogleAuthProvider.credential(data.idToken)).then((data2)=>{
  //       alert('login suc');
  //       console.log(data2);
  //     })
  //   },err=>{
  //     console.log('err',err);
  //   })

  // }
  // loginWithFb(){
  //   console.log('in fb');
  //   this.fb.login(['public_profile','email']).then((res:FacebookLoginResponse)=>{

  //     console.log(res);
  //     this.fb.api('me?fields=id,name,email,first_name,picture',['public_profile','email']).then(profile =>{
  //         //this.userData={email: profile['email'],first_name: profile['first_name'],picture: profile['picture_large']['data']['url'],username: profile['name']}
  //           console.log('profile data',profile);
  //     });

  //   },error=>{
  //     console.log(error);
  //   });
  // }
}
