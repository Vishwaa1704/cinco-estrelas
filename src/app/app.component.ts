import { Component, ViewChild } from "@angular/core";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { Storage } from "@ionic/storage";
import { Nav, Platform } from "ionic-angular";

import { CategoriesPage } from "../pages/categories/categories";
import { HomePage } from "../pages/home/home";
import { SlidesPage } from "./slides/slides";
import { TranslateService } from "@ngx-translate/core";
import { TouchID } from "@ionic-native/touch-id";
import { FcmProvider } from "../providers/fcm/fcm";

import { ToastController } from "ionic-angular";
import { Subject } from "rxjs/Subject";
import { tap } from "rxjs/operators";

// import { ListPage } from '../pages/list/list';
// import { EditprofilePage } from '../pages/editprofile/editprofile';
// import { WelcomePage } from '../pages/welcome/welcome';
@Component({
  templateUrl: "app.html"
})
export class MyApp {
  @ViewChild(Nav)
  nav: Nav;

  rootPage: any = SlidesPage;

  pages: Array<{ title: string; component: any }>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public storage: Storage,
    translate: TranslateService,
    public fcm: FcmProvider,
    public toastCtrl: ToastController
  ) {
    this.initializeApp();
    this.storage.set("language", 0);
    translate.setDefaultLang("pt");
    // used for an example of ngFor and navigation
    this.pages = [
      { title: "Home", component: HomePage },
      // { title: 'List', component: ListPage },
      // {title:'Edit Profile',component:EditprofilePage},
      // {title:'welcome',component:WelcomePage},
      { title: "slides", component: SlidesPage },
      { title: "categories", component: CategoriesPage }
    ];
  }

  initializeApp() {
    let touchId: TouchID;
    if (touchId) {
      touchId
        .isAvailable()
        .then(
          res => console.log("touchId is available!"),
          err => console.error("touchId is not available", err)
        );

      touchId
        .verifyFingerprint("Scan your fingerprint please")
        .then(
          res => console.log("Ok", res),
          err => console.error("Error", err)
        );
    }

    this.splashScreen.show();
    this.platform.ready().then(() => {
      // Get a FCM token
      this.fcm.getToken();
      // Listen to incoming messages
      this.fcm
        .listenToNotifications()
        .pipe(
          tap(msg => {
            // show a toast
            const toast = this.toastCtrl.create({
              message: msg.body,
              duration: 5000
            });
            toast.present();
          })
        )
        .subscribe();
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
