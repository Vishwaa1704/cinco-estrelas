import { TranslateHttpLoader } from '@ngx-translate/http-loader';
// import { HttpModule, Http } from '@angular/http';
// import { HttpClientModule } from  '@angular/common/http';

export function createTranslateLoader(http: any) {
  return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}
