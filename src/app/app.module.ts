import { HttpClientModule, HttpClient } from "@angular/common/http";
import { ErrorHandler, NgModule } from "@angular/core";
import { Http, HttpModule } from "@angular/http";
import { BrowserModule } from "@angular/platform-browser";
import { Camera } from "@ionic-native/camera";
import { Facebook } from "@ionic-native/facebook";
import { Geolocation } from "@ionic-native/geolocation";
import { GoogleMaps } from "@ionic-native/google-maps";
import { GooglePlus } from "@ionic-native/google-plus";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { IonicStorageModule } from "@ionic/storage";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { AngularFireModule } from "angularfire2";
import { AngularFireAuthModule } from "angularfire2/auth";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";

import { SocialSharing } from "@ionic-native/social-sharing";
import { NativeStorage } from '@ionic-native/native-storage';

import { AboutPage } from "../pages/about/about";
import { AccepttermsPage } from "../pages/acceptterms/acceptterms";
import { AddLocationsPage } from "../pages/addLocations/addLocations";
import { CancelaccountPage } from "../pages/cancelaccount/cancelaccount";
import { CategoriesPage } from "../pages/categories/categories";
import { ChangepasswordPage } from "../pages/changepassword/changepassword";
import { CommentPage } from "../pages/comment/comment";
import { ConfirmdeletePage } from "../pages/confirmdelete/confirmdelete";
import { ContactPage } from "../pages/contact/contact";
import { EnableLocalizationPage } from "../pages/enable-localization/enable-localization";
import { EnableNotificationPage } from "../pages/enable-notification/enable-notification";
import { FavouritesPage } from "../pages/favourites/favourites";
import { FeedbacksuccessPage } from "../pages/feedbacksuccess/feedbacksuccess";
import { HomePage } from "../pages/home/home";
import { LanguagePage } from "../pages/language/language";
import { LeavecommentPage } from "../pages/leavecomment/leavecomment";
import { ListPage } from "../pages/list/list";
import { ManageUsersPage } from "../pages/manageUsers/manageUsers";
import { MapsexPage } from "../pages/mapsex/mapsex";
import { MenuPage } from "../pages/menu/menu";
import { ProfilePage } from "../pages/profile/profile";
import { RecoverynewpasswordPage } from "../pages/recoverynewpassword/recoverynewpassword";
import { RecoverypasswordOtpPage } from "../pages/recoverypassword-otp/recoverypassword-otp";
import { RecoverypasswordPage } from "../pages/recoverypassword/recoverypassword";
import { RegisterOtpPage } from "../pages/register-otp/register-otp";
import { RegistrationPage } from "../pages/registration/registration";
import { TabsPage } from "../pages/tabs/tabs";
import { TermsconditionPage } from "../pages/termscondition/termscondition";
import { UpdateUserByAdminPage } from "../pages/updateUserByAdmin/updateUserByAdmin";
import { WelcomePage } from "../pages/welcome/welcome";
import { SearchPage } from "../pages/searchPage/searchPage";
import { PromotionsPage } from "../pages/promotions/promotions";
import { LocationsListViewPage } from "../pages/locationsListView/locationsListView";
import { LocationDetailsPage } from "../pages/locationDetails/locationDetails";
import { MarkerProvider } from "../providers/marker/marker";
import { RestProvider } from "../providers/rest/rest";
import { UserProvider } from "../providers/user/user";
import { AlertProvider } from "../providers/alert/alert";
import { MapProvider } from "../providers/map/map";
import { CompanyProviders } from '../providers/companies/companies';
import { CustomTag } from "../pages/customTag/customTag";
import { MyApp } from "./app.component";
import { SlidesPage } from "./slides/slides";
import { createTranslateLoader } from "./translator";
import { TouchID } from "@ionic-native/touch-id";
import { CompanyCommentPage } from "../pages/companyComment/companyComment";
import { NotificationPage } from "../pages/notification/notification";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { FcmProvider } from "../providers/fcm/fcm";
import { Firebase } from "@ionic-native/firebase";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { ActivePagesProvider } from '../providers/activePages/activePages';

const firebaseConfig = {
  apiKey: "AIzaSyCxCtT9W_Y_kzfqc5dNiFcoQR5LRvb-JkM",
  authDomain: "fir-poc-4fdc4.firebaseapp.com",
  databaseURL: "https://fir-poc-4fdc4.firebaseio.com",
  projectId: "fir-poc-4fdc4",
  storageBucket: "fir-poc-4fdc4.appspot.com",
  messagingSenderId: "906164390641"
};

@NgModule({
  declarations: [
    MyApp,
    SlidesPage,
    HomePage,
    RecoverypasswordPage,
    RecoverypasswordOtpPage,
    RecoverynewpasswordPage,
    RegistrationPage,
    RegisterOtpPage,
    AccepttermsPage,
    TermsconditionPage,
    WelcomePage,
    EnableNotificationPage,
    EnableLocalizationPage,
    CategoriesPage,
    MenuPage,
    ProfilePage,
    FavouritesPage,
    ContactPage,
    LanguagePage,
    ChangepasswordPage,
    CancelaccountPage,
    CommentPage,
    ConfirmdeletePage,
    LeavecommentPage,
    FeedbacksuccessPage,
    MapsexPage,
    AboutPage,
    ListPage,
    TabsPage,
    AddLocationsPage,
    ManageUsersPage,
    UpdateUserByAdminPage,
    LocationsListViewPage,
    LocationDetailsPage,
    SearchPage,
    PromotionsPage,
    CompanyCommentPage,
    NotificationPage,
    CustomTag
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      mode: "ios"
    }),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,
    HttpModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SlidesPage,
    HomePage,
    RecoverypasswordPage,
    RecoverypasswordOtpPage,
    RecoverynewpasswordPage,
    RegistrationPage,
    RegisterOtpPage,
    AccepttermsPage,
    TermsconditionPage,
    WelcomePage,
    EnableNotificationPage,
    EnableLocalizationPage,
    CategoriesPage,
    MenuPage,
    ProfilePage,
    FavouritesPage,
    ContactPage,
    LanguagePage,
    ChangepasswordPage,
    CancelaccountPage,
    CommentPage,
    ConfirmdeletePage,
    LeavecommentPage,
    FeedbacksuccessPage,
    MapsexPage,
    AboutPage,
    ListPage,
    TabsPage,
    AddLocationsPage,
    ManageUsersPage,
    UpdateUserByAdminPage,
    LocationsListViewPage,
    LocationDetailsPage,
    SearchPage,
    PromotionsPage,
    CompanyCommentPage,
    NotificationPage,
    CustomTag
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    GoogleMaps,
    GooglePlus,
    Facebook,
    Geolocation,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    RestProvider,
    UserProvider,
    MarkerProvider,
    SocialSharing,
    AlertProvider,
    MapProvider,
    TouchID,
    Firebase,
    FcmProvider,
    ActivePagesProvider,
    NativeStorage,
    CompanyProviders
  ]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}
